﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using WeatherChallenge.Insfrastructure;
using WeatherChallenge.Providers.Implementations;
using WeatherChallenge.Providers.Interfaces;

namespace WeatherChallenge.UnitTests.Providers
{
    public class MetaWeatherProviderTests
    {
        private Mock<IRestClient> _restClient;
        [SetUp]
        public void Setup() => _restClient = new Mock<IRestClient>();

        [Test]
        public async Task LocationWithoutWoeid_ShouldReturnNull()
        {
            var fakeLocations = new List<MetaWeatherProvider.MetaWeatherResponse.Location>
            {
                new MetaWeatherProvider.MetaWeatherResponse.Location
                    { Title = "Lima", Woeid = null }
            };

            _restClient
                .Setup(s => s.GetAsync<IEnumerable<MetaWeatherProvider.MetaWeatherResponse.Location>>(It.IsAny<string>(),
                    It.IsAny<Dictionary<string, string>>()))
                .ReturnsAsync(fakeLocations);

            IWeatherProvider provider = new MetaWeatherProvider(_restClient.Object);

            var weatherSummary = await provider.GetWeatherByCoordinatesAsync(It.IsAny<long>(), It.IsAny<long>());

            Assert.IsNull(weatherSummary);
        }

        [Test]
        public async Task EmptyLocationsList_ShouldReturnNull()
        {
            _restClient
                .Setup(s => s.GetAsync<IEnumerable<MetaWeatherProvider.MetaWeatherResponse.Location>>(It.IsAny<string>(),
                    It.IsAny<Dictionary<string, string>>()))
                .ReturnsAsync(Enumerable.Empty<MetaWeatherProvider.MetaWeatherResponse.Location>);

            IWeatherProvider provider = new MetaWeatherProvider(_restClient.Object);

            var weatherSummary = await provider.GetWeatherByCoordinatesAsync(It.IsAny<long>(), It.IsAny<long>());

            Assert.IsNull(weatherSummary);
        }

        [Test]
        public async Task EmptyWeathersList_ShouldReturnWeatherSummaryWithEmptyList()
        {
            var fakeLocations = new List<MetaWeatherProvider.MetaWeatherResponse.Location>
            {
                new MetaWeatherProvider.MetaWeatherResponse.Location
                    { Title = "Lima", Woeid = 1 }
            };

            var fakeWeatherSummary = new MetaWeatherProvider.MetaWeatherResponse.WeatherSummary
                {ConsolidatedWeather = Enumerable.Empty<MetaWeatherProvider.MetaWeatherResponse.Weather>()};

            _restClient
                .Setup(s => s.GetAsync<IEnumerable<MetaWeatherProvider.MetaWeatherResponse.Location>>(It.IsAny<string>(),
                    It.IsAny<Dictionary<string, string>>()))
                .ReturnsAsync(fakeLocations);

            _restClient
                .Setup(s => s.GetAsync<MetaWeatherProvider.MetaWeatherResponse.WeatherSummary>(It.IsAny<string>()))
                .ReturnsAsync(fakeWeatherSummary);

            IWeatherProvider provider = new MetaWeatherProvider(_restClient.Object);

            var weatherSummary = await provider.GetWeatherByCoordinatesAsync(It.IsAny<long>(), It.IsAny<long>());

            Assert.IsEmpty(weatherSummary.Weathers);
        }

        [Test]
        public async Task WeatherList_ShouldReturnWeatherSummaryWithTheSameCount()
        {
            var fakeLocations = new List<MetaWeatherProvider.MetaWeatherResponse.Location>
            {
                new MetaWeatherProvider.MetaWeatherResponse.Location
                    { Title = "Lima", Woeid = 1 }
            };

            var fakeWeatherSummary = new MetaWeatherProvider.MetaWeatherResponse.WeatherSummary
            {
                ConsolidatedWeather = new List<MetaWeatherProvider.MetaWeatherResponse.Weather>
                {
                    new MetaWeatherProvider.MetaWeatherResponse.Weather
                        {Id = 1, HighTemperature = 30, LowTemperature = 20 },
                    new MetaWeatherProvider.MetaWeatherResponse.Weather
                        {Id = 2, HighTemperature = 31, LowTemperature = 21 },
                    new MetaWeatherProvider.MetaWeatherResponse.Weather
                        {Id = 3, HighTemperature = 32, LowTemperature = 22 }
                }
            };

            _restClient
                .Setup(s => s.GetAsync<IEnumerable<MetaWeatherProvider.MetaWeatherResponse.Location>>(It.IsAny<string>(),
                    It.IsAny<Dictionary<string, string>>()))
                .ReturnsAsync(fakeLocations);

            _restClient
                .Setup(s => s.GetAsync<MetaWeatherProvider.MetaWeatherResponse.WeatherSummary>(It.IsAny<string>()))
                .ReturnsAsync(fakeWeatherSummary);

            IWeatherProvider provider = new MetaWeatherProvider(_restClient.Object);

            var weatherSummary = await provider.GetWeatherByCoordinatesAsync(It.IsAny<long>(), It.IsAny<long>());

            Assert.True(weatherSummary.Weathers.Count() == fakeWeatherSummary.ConsolidatedWeather.Count());
        }
    }
}
