﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using WeatherChallenge.Insfrastructure;
using WeatherChallenge.Providers.Implementations;
using WeatherChallenge.Providers.Interfaces;

namespace WeatherChallenge.UnitTests.Providers
{
    public class DarkSkyWeatherProviderTests
    {
        private Mock<IRestClient> _restClient;
        private Mock<IConfiguration> _configuration;

        [SetUp]
        public void Setup()
        {
            _restClient = new Mock<IRestClient>();
            _configuration = new Mock<IConfiguration>();
        }

        [Test]
        public async Task NullResponse_ShouldReturnWeatherSummaryWithEmptyList()
        {
            _restClient
                .Setup(s => s.GetAsync<DarkSkyWeatherProvider.DarkSkyWeatherResponse>(It.IsAny<string>(),
                    It.IsAny<Dictionary<string, string>>()))
                .ReturnsAsync(() => null);

            IWeatherProvider provider = new DarkSkyWeatherProvider(_restClient.Object, _configuration.Object);

            var weatherSummary = await provider.GetWeatherByCoordinatesAsync(It.IsAny<long>(), It.IsAny<long>());

            Assert.NotNull(weatherSummary);
            Assert.IsEmpty(weatherSummary.Weathers);
        }

        [Test]
        public async Task WeatherList_ShouldReturnWeatherSummaryWithTheSameCount()
        {
            var fakeWeatherResponse = new DarkSkyWeatherProvider.DarkSkyWeatherResponse
            {
                Daily = new DarkSkyWeatherProvider.DarkSkyWeatherResponse.DailySummary
                {
                    Data = new List<DarkSkyWeatherProvider.DarkSkyWeatherResponse.WeatherItem>
                    {
                        new DarkSkyWeatherProvider.DarkSkyWeatherResponse.WeatherItem
                            { TemperatureHigh = 30, TemperatureLow = 20 , Time = 123123 }
                    }
                }
            };

            _restClient
                .Setup(s => s.GetAsync<DarkSkyWeatherProvider.DarkSkyWeatherResponse>(It.IsAny<string>(),
                    It.IsAny<Dictionary<string, string>>()))
                .ReturnsAsync(fakeWeatherResponse);

            IWeatherProvider provider = new DarkSkyWeatherProvider(_restClient.Object, _configuration.Object);

            var weatherSummary = await provider.GetWeatherByCoordinatesAsync(It.IsAny<long>(), It.IsAny<long>());

            Assert.True(weatherSummary.Weathers.Count() == fakeWeatherResponse.Daily.Data.Count());
        }

        [Test]
        public async Task WeatherList_ShouldReturnWeatherSummaryApplyingMaxSize()
        {
            int maxSize = 2;

            var fakeWeatherResponseExceedingMaxSize = new DarkSkyWeatherProvider.DarkSkyWeatherResponse
            {
                Daily = new DarkSkyWeatherProvider.DarkSkyWeatherResponse.DailySummary
                {
                    Data = new List<DarkSkyWeatherProvider.DarkSkyWeatherResponse.WeatherItem>
                    {
                        new DarkSkyWeatherProvider.DarkSkyWeatherResponse.WeatherItem
                            { TemperatureHigh = 30, TemperatureLow = 20 , Time = 123123 },
                        new DarkSkyWeatherProvider.DarkSkyWeatherResponse.WeatherItem
                            { TemperatureHigh = 30, TemperatureLow = 20 , Time = 123123 },
                        new DarkSkyWeatherProvider.DarkSkyWeatherResponse.WeatherItem
                            { TemperatureHigh = 30, TemperatureLow = 20 , Time = 123123 }
                    }
                }
            };

            _restClient
                .Setup(s => s.GetAsync<DarkSkyWeatherProvider.DarkSkyWeatherResponse>(It.IsAny<string>(),
                    It.IsAny<Dictionary<string, string>>()))
                .ReturnsAsync(fakeWeatherResponseExceedingMaxSize);

            IWeatherProvider provider = new DarkSkyWeatherProvider(_restClient.Object, _configuration.Object);

            var weatherSummary = await provider.GetWeatherByCoordinatesAsync(It.IsAny<long>(), It.IsAny<long>(), maxSize);

            Assert.True(weatherSummary.Weathers.Count() == maxSize);
        }
    }
}
