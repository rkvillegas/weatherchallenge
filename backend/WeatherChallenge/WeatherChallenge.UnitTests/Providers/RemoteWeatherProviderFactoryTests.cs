using System;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using WeatherChallenge.Insfrastructure;
using WeatherChallenge.Providers.Implementations;
using WeatherChallenge.Providers.Interfaces;

namespace WeatherChallenge.UnitTests.Providers
{
    public class RemoteWeatherProviderFactoryTests
    {
        private IWeatherProviderFactory _weatherProviderFactory;
        [SetUp]
        public void Setup()
        {
            var configuration = new Mock<IConfiguration>();
            var restClient = new Mock<IRestClient>();
            _weatherProviderFactory = new RemoteWeatherProviderFactory(configuration.Object, restClient.Object);
        }

        [Test]
        public void RandomProviderName_ShouldThrowsArgumentException()
        {
            var randomProviderName = Guid.NewGuid().ToString();

            Assert.Throws<ArgumentException>(() => { var _ = _weatherProviderFactory.CreateWeatherProviderName(randomProviderName); });
        }

        [Test]
        public void DarkSkyProviderName_ShouldReturnProperProvider()
        {
            var provider = _weatherProviderFactory.CreateWeatherProviderName("darkskyweather");

            Assert.IsInstanceOf<DarkSkyWeatherProvider>(provider);
        }

        [Test]
        public void MetaWeatherProviderName_ShouldReturnProperProvider()
        {
            var provider = _weatherProviderFactory.CreateWeatherProviderName("metaweather");

            Assert.IsInstanceOf<MetaWeatherProvider>(provider);
        }

        [Test]
        public void OpenWeatherProviderName_ShouldReturnProperProvider()
        {
            var provider = _weatherProviderFactory.CreateWeatherProviderName("openweather");

            Assert.IsInstanceOf<OpenWeatherProvider>(provider);
        }

    }
}