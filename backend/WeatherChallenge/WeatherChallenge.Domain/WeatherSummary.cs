﻿using System.Collections.Generic;
using System.Linq;

namespace WeatherChallenge.Domain
{
    public class WeatherSummary
    {
        public IEnumerable<WeatherItem> Weathers { get; }

        public WeatherSummary()
        {
            Weathers = Enumerable.Empty<WeatherItem>();
        }

        public WeatherSummary(IEnumerable<WeatherItem> weathers)
        {
            Weathers = weathers;
        }
    }
}