namespace WeatherChallenge.Domain
{
    public class WeatherItem
    {
        public long Id { get; set; }

        public double HighTemperature { get; set; }

        public double LowTemperature { get; set; }

        public string ShortDate { get; set; }
    }
}