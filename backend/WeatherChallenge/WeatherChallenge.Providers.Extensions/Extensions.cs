﻿using System.Net.Http;
using Microsoft.Extensions.DependencyInjection;
using WeatherChallenge.Insfrastructure;
using WeatherChallenge.Providers.Implementations;
using WeatherChallenge.Providers.Interfaces;

namespace WeatherChallenge.Providers.Extensions
{
    public static class Extensions
    {
        public static IServiceCollection AddRemoteWeatherProviders(this IServiceCollection services)
        {
            services.AddSingleton(new HttpClient());
            services.AddTransient<IRestClient, RestClient>();
            services.AddTransient<IWeatherProviderFactory, RemoteWeatherProviderFactory>();

            return services;
        }
    }
}
