﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using WeatherChallenge.Insfrastructure;
using WeatherChallenge.Providers.Extensions;

namespace WeatherChallenge.Web.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRemoteWeatherProviders();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // TODO: Write a custom handler to put this logic and use Logger (important!)
            // TODO: because we dont want to do not be able to see de exceptions details
            app.UseExceptionHandler(errorApp =>
            {
                errorApp.Run(async context =>
                {
                    context.Response.StatusCode = 500;
                    context.Response.ContentType = "application/json";

                    var exceptionHandlerPathFeature =
                        context.Features.Get<IExceptionHandlerPathFeature>();

                    HttpErrorDetails errorDetails = null;

                    if (exceptionHandlerPathFeature?.Error is ApiException)
                    {
                        errorDetails =
                            new HttpErrorDetails
                            {
                                StatusCode = context.Response.StatusCode,
                                Message = "The provider API is not working properly"
                            };
                    }
                    else
                    {
                        errorDetails =
                            new HttpErrorDetails
                            {
                                StatusCode = context.Response.StatusCode,
                                Message = "There are internal errors in the server."
                            };
                    }

                    await context.Response.WriteAsync(JsonConvert.SerializeObject(errorDetails));
                });
            });

            if (!env.IsDevelopment())
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
