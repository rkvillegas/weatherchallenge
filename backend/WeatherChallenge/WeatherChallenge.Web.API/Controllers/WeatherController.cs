﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WeatherChallenge.Providers.Interfaces;

namespace WeatherChallenge.Web.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WeatherController : ControllerBase
    {
        private readonly IWeatherProviderFactory _weatherProviderFactory;

        public WeatherController(IWeatherProviderFactory weatherProviderFactory) 
            => _weatherProviderFactory = weatherProviderFactory;


        [HttpGet("{provider}/{latitude}/{longitude}")]
        public async Task<ActionResult> Get(string provider, double latitude, double longitude)
        {
            var weatherSummary = await _weatherProviderFactory
                .CreateWeatherProviderName(provider)
                .GetWeatherByCoordinatesAsync(latitude, longitude);
            
            return new OkObjectResult(weatherSummary);
        }
      
    }
}