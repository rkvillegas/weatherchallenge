# Code challenge backend

## Project setup
````
cd ./WeatherChallenge/ && dotnet restore
````
### Builds the project
````
dotnet build
````
### Run the project
````
cd ./WeatherChallenge.Web.API && dotnet run
````
### Run the tests
Go back to solution root folder and run the following command:
```
dotnet test
```

## TODO
* Complete writing the 3rd provider implementation (OpenWeather)
