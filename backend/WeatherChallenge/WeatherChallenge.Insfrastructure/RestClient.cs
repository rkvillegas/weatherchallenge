using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;

namespace WeatherChallenge.Insfrastructure
{
    
    public class RestClient : IRestClient
    {
        private readonly HttpClient _httpClient;

        HttpClient IRestClient.HttpClient => _httpClient;

        public RestClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        private static string ParseUri(string url, IDictionary<string, string> query)
        {
            var queryString = HttpUtility.ParseQueryString(string.Empty);
            foreach (var value in query)
                queryString[value.Key.ToLower()] = value.Value;

            var q = queryString.ToString();
            var builder = new UriBuilder(url.ToLower());
            if (!string.IsNullOrEmpty(q))
                builder.Query = q;

            return builder.ToString();
        }
        

        async Task<T> IRestClient.GetAsync<T>(string url)
        {
            var response = await _httpClient.GetAsync(url);

            if (response.IsSuccessStatusCode)
            {
                if (response.IsJson())
                {
                    var rawContent = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<T>(rawContent);
                }

                throw new ApiException($"URL: '{url}' did not return the expected content type", response);
            }


            return default(T);
        }
        
        async Task<T> IRestClient.GetAsync<T>(string url, IDictionary<string, string> query)
        {
            var builder = ParseUri(url, query);

            return await ((IRestClient)this).GetAsync<T>(builder);
        }
    }
}