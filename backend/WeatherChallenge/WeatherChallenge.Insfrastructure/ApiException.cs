using System;
using System.Net;
using System.Net.Http;

namespace WeatherChallenge.Insfrastructure
{
    public sealed class ApiException : Exception
    {
        public HttpResponseMessage Response { get; }

        public HttpStatusCode StatusCode { get; }

        public ApiException(string message, HttpResponseMessage response)
            : base(message)
        {
            Response = response;
            StatusCode = Response.StatusCode;
        }
    }
}