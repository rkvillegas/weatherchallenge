﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherChallenge.Insfrastructure
{
    public class HttpErrorDetails
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}
