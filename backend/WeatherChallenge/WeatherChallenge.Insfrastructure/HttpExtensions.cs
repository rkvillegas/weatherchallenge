using System.Net.Http;

namespace WeatherChallenge.Insfrastructure
{
    public static class HttpExtensions
    {
        public static bool IsJson(this HttpResponseMessage response)
            => response.Content.Headers.ContentType.MediaType == "application/json";
    }
}