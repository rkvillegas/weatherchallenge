using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace WeatherChallenge.Insfrastructure
{
    public interface IRestClient
    {
        HttpClient HttpClient { get; }

        Task<T> GetAsync<T>(string url);

        Task<T> GetAsync<T>(string url, IDictionary<string, string> query);
    }
}