﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using WeatherChallenge.Domain;
using WeatherChallenge.Insfrastructure;
using WeatherChallenge.Providers.Interfaces;

namespace WeatherChallenge.Providers.Implementations
{
    public class OpenWeatherProvider : IWeatherProvider
    {
        private readonly IRestClient _restClient;

        private readonly string _apiKey;

        private readonly string __baseUri = "https://api.openweathermap.org/data/2.5";

        private readonly IMapper _mapper;

        public OpenWeatherProvider(IRestClient restClient, IConfiguration config)
        {
            _restClient = restClient;
            _apiKey = config["OpenWeatherApiKey"];
            _mapper = GetMapperConfiguration().CreateMapper();
        }

        // TODO: TBD
        Task<WeatherSummary> IWeatherProvider.GetWeatherByCoordinatesAsync(double latitude, double longitude, int maxSize) =>
            throw new NotImplementedException();

        private static MapperConfiguration GetMapperConfiguration()
            => new MapperConfiguration(cfg => { });

    }
}
