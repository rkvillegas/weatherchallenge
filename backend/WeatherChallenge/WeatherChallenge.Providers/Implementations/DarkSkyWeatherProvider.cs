﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using WeatherChallenge.Domain;
using WeatherChallenge.Insfrastructure;
using WeatherChallenge.Providers.Interfaces;

namespace WeatherChallenge.Providers.Implementations
{
    public class DarkSkyWeatherProvider : IWeatherProvider
    {
        private readonly IRestClient _restClient;

        private readonly string _apiKey;

        private readonly string __baseUri = "https://api.darksky.net";

        private readonly IMapper _mapper;

        public DarkSkyWeatherProvider(IRestClient restClient, IConfiguration config)
        {
            _restClient = restClient;
            _apiKey = config["DarkSkyWeatherApiKey"];
            _mapper = GetMapperConfiguration().CreateMapper();
        }

        async Task<WeatherSummary> IWeatherProvider.GetWeatherByCoordinatesAsync(double latitude, double longitude, int maxSize)
        {
            var weatherResponse = await _restClient
                .GetAsync<DarkSkyWeatherResponse>(
                    $"{__baseUri}/forecast/{_apiKey}/{latitude},{longitude}",
                    new Dictionary<string, string>
                    {
                        {"units", "si"},
                        {"exclude", "currently,minutely,hourly,alerts,flags"}
                    });

            return weatherResponse == null
                ? new WeatherSummary()
                : new WeatherSummary(_mapper.Map<IEnumerable<WeatherItem>>(weatherResponse.Daily.Data)
                    .Take(maxSize));
        }

        private static MapperConfiguration GetMapperConfiguration()
            => new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<DarkSkyWeatherResponse, WeatherSummary>()
                    .ForMember(d => d.Weathers,
                        opt => opt.MapFrom(s => s.Daily))
                    .ReverseMap();

                // TODO: We should be carefully about setting timezoneOffset. But it is not needed now
                // TODO: because we are only dealing with date
                cfg.CreateMap<DarkSkyWeatherResponse.WeatherItem, WeatherItem>()
                    .ForMember(d => d.Id,
                        opt => opt.MapFrom(s => s.Time))
                    .ForMember(d => d.ShortDate,
                        opt => opt.MapFrom(s =>
                            DateTimeOffset.FromUnixTimeSeconds(s.Time).Date.ToShortDateString()))
                    .ForMember(d => d.HighTemperature,
                        opt => opt.MapFrom(s => s.TemperatureHigh))
                    .ForMember(d => d.LowTemperature,
                        opt => opt.MapFrom(s => s.TemperatureLow))
                    .ReverseMap();
            });

        // TODO: Move these classes to WeatherChallenge.Providers.Api.Models
        public class DarkSkyWeatherResponse
        {
            public DailySummary Daily { get; set; }
            [JsonProperty("offset")]
            public int TimezoneOffset { get; set; }
            public class DailySummary
            {
                public IEnumerable<WeatherItem> Data { get; set; }
            }

            public class WeatherItem
            {
                public double TemperatureHigh { get; set; }
                public double TemperatureLow { get; set; }
                public long Time { get; set; }
            }
        }
    }
}
