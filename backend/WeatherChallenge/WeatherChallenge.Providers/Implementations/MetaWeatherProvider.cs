﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Newtonsoft.Json;
using WeatherChallenge.Domain;
using WeatherChallenge.Insfrastructure;
using WeatherChallenge.Providers.Interfaces;

namespace WeatherChallenge.Providers.Implementations
{
    public class MetaWeatherProvider : IWeatherProvider
    {
        private readonly IRestClient _restClient;

        private readonly string __baseUri = "https://www.metaweather.com/api";

        private readonly IMapper _mapper;

        public MetaWeatherProvider(IRestClient restClient)
        {
            _restClient = restClient;
            _mapper = GetMapperConfiguration().CreateMapper();
        }

        async Task<WeatherSummary> IWeatherProvider.GetWeatherByCoordinatesAsync(double latitude, double longitude,
            int maxSize)
        {
            var foundLocation = await SearchLocationByCoordinatesAsync(latitude, longitude);

            if (foundLocation?.Woeid == null)
                return null;

            var weatherSummary = await GetWeathersByLocationIdAsync(foundLocation.Woeid.Value);

            return !weatherSummary.ConsolidatedWeather.Any()
                ? new WeatherSummary()
                : new WeatherSummary(_mapper.Map<IEnumerable<WeatherItem>>(weatherSummary.ConsolidatedWeather)
                    .Take(maxSize));
        }

        private Task<MetaWeatherResponse.WeatherSummary> GetWeathersByLocationIdAsync(long locationId)
            => _restClient
                .GetAsync<MetaWeatherResponse.WeatherSummary>(
                    $"{__baseUri}/location/{locationId}");

        private async Task<MetaWeatherResponse.Location> SearchLocationByCoordinatesAsync(double latitude,
            double longitude)
        {
            var locations = await _restClient
                .GetAsync<IEnumerable<MetaWeatherResponse.Location>>(
                    $"{__baseUri}/location/search",
                    new Dictionary<string, string>
                    {
                        {"lattlong", $"{latitude},{longitude}"}
                    });

            return locations.FirstOrDefault();
        }

        private static MapperConfiguration GetMapperConfiguration()
            => new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MetaWeatherResponse.WeatherSummary, WeatherSummary>()
                    .ForMember(d => d.Weathers,
                        opt => opt.MapFrom(s => s.ConsolidatedWeather))
                    .ReverseMap();

                cfg.CreateMap<MetaWeatherResponse.Weather, WeatherItem>()
                    .ForMember(d => d.ShortDate,
                        opt => opt.MapFrom(s =>
                            DateTime.ParseExact(s.ApplicableDate, "yyyy-MM-dd", CultureInfo.InvariantCulture)
                                .ToShortDateString()))
                    .ReverseMap();
            });

        // TODO: Move these classes to WeatherChallenge.Providers.Api.Models
        public class MetaWeatherResponse
        {
            public class Location
            {
                public long? Woeid { get; set; }
                public string Title { get; set; }
            }

            public class WeatherSummary
            {
                [JsonProperty("consolidated_weather")] public IEnumerable<Weather> ConsolidatedWeather { get; set; }
            }

            public class Weather
            {
                public long Id { get; set; }
                [JsonProperty("max_temp")] public double HighTemperature { get; set; }
                [JsonProperty("min_temp")] public double LowTemperature { get; set; }
                [JsonProperty("applicable_date")] public string ApplicableDate { get; set; }
            }
        }
    }
}
