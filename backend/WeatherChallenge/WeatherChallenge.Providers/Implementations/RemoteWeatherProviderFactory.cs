﻿using System;
using Microsoft.Extensions.Configuration;
using WeatherChallenge.Insfrastructure;
using WeatherChallenge.Providers.Interfaces;

namespace WeatherChallenge.Providers.Implementations
{
    public class RemoteWeatherProviderFactory : IWeatherProviderFactory
    {
        private readonly IConfiguration _configuration;
        private readonly IRestClient _restClient;

        public RemoteWeatherProviderFactory(IConfiguration configuration, IRestClient restClient)
        {
            _configuration = configuration;
            _restClient = restClient;
        }

        // TODO: We should go in strongly typed way (ex: Enums)
        IWeatherProvider IWeatherProviderFactory.CreateWeatherProviderName(string providerName)
        {
            switch (providerName)
            {
                case "metaweather":
                    return new MetaWeatherProvider(_restClient);
                case "openweather":
                    return new OpenWeatherProvider(_restClient, _configuration);
                case "darkskyweather":
                    return new DarkSkyWeatherProvider(_restClient, _configuration);
                default:
                    throw new ArgumentException("Incorrect provider", nameof(providerName));
            }
        }
    }
}
