﻿using System.Threading.Tasks;
using WeatherChallenge.Domain;

namespace WeatherChallenge.Providers.Interfaces
{
    public interface IWeatherProvider
    {
        //TODO: Configure maxSize as custom setting variable and use a nullable here
        Task<WeatherSummary> GetWeatherByCoordinatesAsync(double latitude, double longitude, int maxSize = 6);
    }
}
