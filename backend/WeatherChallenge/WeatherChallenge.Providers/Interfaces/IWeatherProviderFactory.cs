namespace WeatherChallenge.Providers.Interfaces
{
    public interface IWeatherProviderFactory
    {
        IWeatherProvider CreateWeatherProviderName(string provider);
    }
}