import {factory} from "../../test.utils";
import WeathersListItem from "@/components/weathers/list/weathers.list.item";

describe("Test WeathersListItem Component", () => {
    const fakeWeather = { shortDate: "10/6", highTemperature: 20, lowTemperature: 15 };

    test("Weather's props should be rendered properly", () => {

        const wrapper = factory(WeathersListItem, {}, { isLoading: false, weather: fakeWeather });

        const shortDateEl = wrapper.find(".card-header-title");
        const highTempEl = wrapper.find(".content>p:nth-child(1)");
        const lowTempEl = wrapper.find(".content>p:nth-child(2)");

        expect(shortDateEl.text().includes(fakeWeather.shortDate)).toBeTruthy();
        expect(highTempEl.text().includes(fakeWeather.highTemperature)).toBeTruthy();
        expect(lowTempEl.text().includes(fakeWeather.lowTemperature)).toBeTruthy();
    });

    test("b-loading component should be active when isLoading prop is true", () => {

        const wrapper = factory(WeathersListItem, {}, { isLoading: true, weather: fakeWeather });

        const loadingSpinEl = wrapper.find("b-loading");

        expect(loadingSpinEl.attributes().active).toBeTruthy();
    })

});

