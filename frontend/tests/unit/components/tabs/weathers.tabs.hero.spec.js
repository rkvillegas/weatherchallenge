import { factory } from "../../test.utils"
import WeathersTabsHero from "@/components/weathers/tabs/weathers.tabs.hero";

describe("Test WeathersTabHero Component", () => {
    test("Passed props values should be rendered in their proper title element", () => {
        const fakeWeather = { highTemperature: 10, lowTemperature: 5 };
        const fakeTitle = "FooBar";

        const wrapper = factory(WeathersTabsHero, {}, { title: fakeTitle, weather: fakeWeather });
        console.log(wrapper.html());

        const titleEl = wrapper.find("h1:nth-child(2)");
        const highTemperatureEl = wrapper.find("h1:nth-child(3)");
        const lowTemperatureEl = wrapper.find("h1:nth-child(4)");

        expect(titleEl.text().includes(fakeTitle)).toBeTruthy();
        expect(highTemperatureEl.text().includes(fakeWeather.highTemperature)).toBeTruthy();
        expect(lowTemperatureEl.text().includes(fakeWeather.lowTemperature)).toBeTruthy();
    })
});