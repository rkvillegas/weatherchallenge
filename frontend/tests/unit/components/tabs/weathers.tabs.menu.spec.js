import {factory} from "../../test.utils";
import WeathersTabsMenu from "@/components/weathers/tabs/weathers.tabs.menu";

describe("Test WeathersTabsMenu Component", () => {
    test("Title should be rendered properly", () => {

        const fakeProvider = {id: 1, keyName: "fooKeyName", title: "fooTitle"};
        const wrapper = factory(WeathersTabsMenu, {}, {currentTab: fakeProvider.keyName, providers: [fakeProvider]});

        const titleEl = wrapper.find("li:nth-child(1)");

        expect(titleEl.text().includes(fakeProvider.title)).toBeTruthy();
    });

    test("li element should be rendered the same count of fakeProviders passed", () => {

        const fakeProviders = [
            {id: 1, keyName: "fooKeyName", title: "fooTitle"},
            {id: 2, keyName: "fooKeyName2", title: "fooTitle2"},
            {id: 3, keyName: "fooKeyName3", title: "fooTitle"},
            ];

        const wrapper = factory(WeathersTabsMenu, {}, { currentTab: "foo", providers: fakeProviders });

        const renderedProviderEls = wrapper.findAll("li");

        expect(renderedProviderEls.length).toBe(fakeProviders.length);
    });

    test("'on-provider-selected' event must be emitted with clickEvent", () => {
        const fakeProvider = {id: 1, keyName: "fooKeyName", title: "fooTitle"};
        const wrapper = factory(WeathersTabsMenu, {}, {currentTab: fakeProvider.keyName, providers: [fakeProvider]});

        const titleEl = wrapper.find("li:nth-child(1)");
        titleEl.trigger("click");

        expect(wrapper.emitted()).toBeTruthy();
    });
});
