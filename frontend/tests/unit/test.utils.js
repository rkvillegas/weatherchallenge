import { shallowMount } from "@vue/test-utils";

const factory = (component, data = {}, propsData = {}) => {
    return shallowMount(component, {
        data: () => {
            return {...data}
        },
        propsData: {...propsData}
    })
};

export {
    factory
};
