import WeathersMixin from "@/mixins/weathers.mixin";

describe("Test Weathers mixin", () => {
    test("Null temperature should return empty string", () => {
        const nullTemperature = null;

        const styleClass = WeathersMixin.methods.getStyleClassByTemperature(nullTemperature);

        expect(styleClass).toBe("");
    });

    test("Temperature less than 15 should return cold-temp", () => {
        const temperature = 10;

        const styleClass = WeathersMixin.methods.getStyleClassByTemperature(temperature);

        expect(styleClass).toBe("cold-temp");
    });

    test("Temperature equal or greater than 15 and less than 20 should return warm-temp", () => {
        const temperature = 17;

        const styleClass = WeathersMixin.methods.getStyleClassByTemperature(temperature);

        expect(styleClass).toBe("warm-temp");
    });

    test("Temperature equal or greater than 20 should return hot-temp", () => {
        const temperature = 30;

        const styleClass = WeathersMixin.methods.getStyleClassByTemperature(temperature);

        expect(styleClass).toBe("hot-temp");
    });

    test("Decimal should be rounded to 2 zeros", () => {
        const numberToBeRounded = 20.136666;

        const roundedNumber = WeathersMixin.filters.round(numberToBeRounded);

        expect(roundedNumber).toBe(20.14);
    });
});
