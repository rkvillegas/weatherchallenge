import Vue from 'vue'
import VueRouter from 'vue-router'
import WeathersPage from './pages/weathers.page.vue'

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    base: __dirname,
    routes: [
        { path: '/weathers', name: 'weathers', component: WeathersPage },
        { path: '*', name: 'home', redirect: '/weathers' },
    ]
});

export default router
