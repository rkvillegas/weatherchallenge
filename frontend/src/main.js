import Vue from 'vue'

import router from './router'
import store from './store/store'

import App from './app.vue'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import axios from 'axios'

Vue.config.productionTip = false;

Vue.use(Buefy);

// TODO: We could import all our custom components here for using in all our application

const vm = new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');

axios.interceptors.response.use(response => response,
   error => {
     vm.$toast.open({
          duration: 5000,
          message: `API error. ${error.response.data.Message}`,
          position: "is-bottom",
          type: "is-danger"
        });
  });
