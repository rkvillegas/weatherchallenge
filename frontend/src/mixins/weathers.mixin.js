export default {
    methods: {
        getStyleClassByTemperature(temperature) {
            if (temperature != null && temperature < 15) {
                return 'cold-temp';
            } else if (temperature >= 15 && temperature < 20) {
                return 'warm-temp';
            } else if (temperature >= 20) {
                return 'hot-temp';
            } else {
                return '';
            }
        }
    },
    filters: {
        round: (numberToRound) => {
            return Math.round(numberToRound * 100) / 100;
        }
    }
}
