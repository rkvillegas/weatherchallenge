import Vue from 'vue'
import Vuex from 'vuex'
import weathersModule from './modules/weathers'

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        weather: weathersModule
    }
});

export default store
