export default {
    state() {
        return {
            providers: [
                { id: 1, keyName: "darkskyweather", title: "DarkSky"  },
                { id: 2, keyName: "metaweather", title: "MetaWeather"  },
            ]
        }
    }
}
