# Code challenge frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### TODO
* Remove warn messages from npm test output. (Import the b-loading component directly into our component for its usage)
* mock axios and test 'weathers.page.vue'
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
